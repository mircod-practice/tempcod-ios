
import UIKit

public class CustomSegmentedControl: UISegmentedControl {
    public func configureSegmentedControl() -> UISegmentedControl {
        backgroundColor = .customLightGray
        selectedSegmentTintColor = .customBlue
        
        let attributesForNormal: [NSAttributedString.Key: Any] = [
            .font: UIFont.label,
            .foregroundColor: UIColor.customBlack
        ]
        
        let attributesForSelected: [NSAttributedString.Key: Any] = [
            .font: UIFont.label,
            .foregroundColor: UIColor.customWhite
        ]
        
        setTitleTextAttributes(attributesForNormal, for: .normal)
        setTitleTextAttributes(attributesForSelected, for: .selected)
        return self
    }
}
