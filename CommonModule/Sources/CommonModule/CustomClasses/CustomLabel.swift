
import UIKit

public class CustomLabel: UILabel {
    public func configureLabel(text: String, fontSize: CGFloat) -> UILabel {
        self.text = text
        self.font = .label.withSize(fontSize)
        self.numberOfLines = 2
        self.textAlignment = .center
        return self
    }
    
    public func configureBlueLabel(username: String) -> UILabel {
        self.text = username
        self.font = .label
        self.textColor = .customBlue
        self.textAlignment = .center
        return self
    }
}
