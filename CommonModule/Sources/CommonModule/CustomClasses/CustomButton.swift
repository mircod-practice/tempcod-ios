import UIKit

public class CustomButton: UIButton {
    public func configureColoredButton(title: String, color: UIColor, sideInset: CGFloat) -> UIButton {
        var configuration = UIButton.Configuration.filled()
        configuration.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: sideInset, bottom: 20, trailing: sideInset)
        configuration.background.cornerRadius = 35
        configuration.baseForegroundColor = .customWhite
        configuration.baseBackgroundColor = color
        configuration.attributedTitle = AttributedString(title, attributes: AttributeContainer([NSAttributedString.Key.font: UIFont.label]))
        self.configuration = configuration
        return self
    }
    
    public func configureSimpleButton(title: String) -> UIButton {
        var configuration = UIButton.Configuration.filled()
        configuration.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: 30, bottom: 20, trailing: 30)
        configuration.baseForegroundColor = .customBlack
        configuration.baseBackgroundColor = .clear
        configuration.attributedTitle = AttributedString(title, attributes: AttributeContainer([NSAttributedString.Key.font: UIFont.label]))
        self.configuration = configuration
        
        let simpleLine = CALayer()
        simpleLine.borderColor = UIColor.customBlack.cgColor;
        simpleLine.borderWidth = 1;
        simpleLine.frame = CGRect(x: 0, y: 53, width: 140, height: 1)
        self.layer.addSublayer(simpleLine)
        return self
    }
    
    public func configureBackButton(title: String) -> UIButton {
        var configuration = UIButton.Configuration.filled()
        configuration.baseForegroundColor = .customBlack
        configuration.baseBackgroundColor = .clear
        configuration.attributedTitle = AttributedString(title , attributes: AttributeContainer([NSAttributedString.Key.font: UIFont.label.withSize(26)]))
        configuration.imagePadding = 20
        self.configuration = configuration
        
        let chevronConfiguration = UIImage.SymbolConfiguration(pointSize: 17, weight: .medium)
        setImage(UIImage(systemName: "chevron.left", withConfiguration: chevronConfiguration), for: .normal)
        return self
    }
}
