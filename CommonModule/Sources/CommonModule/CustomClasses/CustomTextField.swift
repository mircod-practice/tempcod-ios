
import UIKit

public class CustomTextField: UITextField {
    var textPadding = UIEdgeInsets(top: 0, left: 9, bottom: 0, right: 0)

    public override func textRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.textRect(forBounds: bounds)
        return rect.inset(by: textPadding)
    }

    public override func editingRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.editingRect(forBounds: bounds)
        return rect.inset(by: textPadding)
    }
    
    public func configureTextField(placeholder: String) -> UITextField {
        attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: UIColor.customLightGray])
        borderStyle = .roundedRect
        layer.borderColor = UIColor.customLightGray.cgColor
        font = .placeholder
        return self
    }
}
