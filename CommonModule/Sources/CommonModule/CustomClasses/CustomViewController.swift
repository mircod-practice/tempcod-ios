
import UIKit
import SnapKit

public class CustomViewController: UIViewController {
    
    // MARK: - UI Components
    
    public var dialogStateImageView: UIImageView = UIImageView()
    public var largeLabel: UILabel = CustomLabel().configureLabel(text: "", fontSize: 26)
    public var smallLabel: UILabel = CustomLabel().configureLabel(text: "", fontSize: 16)
    public var coloredButton: UIButton = CustomButton().configureColoredButton(title: "", color: .customBlue, sideInset: 50)
    public var simpleButton: UIButton = CustomButton().configureSimpleButton(title: "")
        
    // MARK: - Init
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    // MARK: - Private
    
    private func configureView() {
        view.backgroundColor = .customWhite.withAlphaComponent(0.97)
        addViews()
        configureLayouts()
    }
    
    private func addViews() {
        view.addSubview(dialogStateImageView)
        view.addSubview(largeLabel)
        view.addSubview(smallLabel)
        view.addSubview(coloredButton)
        view.addSubview(simpleButton)
    }
    
    private func configureLayouts() {
        dialogStateImageView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(58)
            make.width.height.equalTo(71)
            make.centerX.equalTo(view.snp.centerX)
        }
        largeLabel.snp.makeConstraints { make in
            make.top.equalTo(dialogStateImageView.snp.bottom).inset(-38)
            make.left.right.equalToSuperview().inset(16)
            make.centerX.equalTo(view.snp.centerX)

        }
        smallLabel.snp.makeConstraints { make in
            make.top.equalTo(largeLabel.snp.bottom).inset(-38)
            make.left.right.equalToSuperview().inset(16)
            make.centerX.equalTo(view.snp.centerX)
        }
        coloredButton.snp.makeConstraints { make in
            make.top.equalTo(smallLabel.snp.bottom).inset(-38)
            make.left.right.equalToSuperview().inset(47)
            make.centerX.equalTo(view.snp.centerX)
        }
        simpleButton.snp.makeConstraints { make in
            make.top.equalTo(coloredButton.snp.bottom).inset(-38)
            make.left.right.equalToSuperview().inset(120)
            make.centerX.equalTo(view.snp.centerX)
        }
    }
}
