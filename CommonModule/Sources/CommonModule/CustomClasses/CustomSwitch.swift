
import UIKit

public class CustomSwitch: UISwitch {
    public func configureSwitch() -> UISwitch {
        tintColor = .customGray
        onTintColor = .customBlue
        return self
    }
}
