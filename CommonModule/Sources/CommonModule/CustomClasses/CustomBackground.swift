
import UIKit

public class CustomBackground: UIView {
    public func configureCustomBackground(isGearshapeNeeded: Bool, view: UIView) -> UIView {
        view.clipsToBounds = true
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [UIColor.customWhite.cgColor, UIColor.customLightGray.cgColor]
        gradientLayer.shouldRasterize = true
        layer.addSublayer(gradientLayer)
        
        if isGearshapeNeeded {
            let gearImage = UIImageView(image: UIImage(systemName: "gearshape.fill")?.withRenderingMode(.alwaysTemplate))
            gearImage.tintColor = .customGray.withAlphaComponent(0.15)
            gearImage.frame = view.alignmentRect(forFrame: CGRect(x: 240, y: 590, width: 230, height: 230))
            addSubview(gearImage)
        }
        
        return self
    }
}
