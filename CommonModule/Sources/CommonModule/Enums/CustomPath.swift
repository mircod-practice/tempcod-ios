
import Foundation

public enum CustomPath: String {
    case absolutePath = "http://3.127.38.160/api/v1"
    case signUpPath = "/auth/signup/"
    case signInPath = "/auth/signin/"
    case logoutPath = "/auth/logout/"
    case verifyTokenPath = "/auth/token/verify/"
    case connectDevicePath = "/device/"
    case sendTempPath = "/temp/"
    case getCurrentUserPath = "/user/current"
}
