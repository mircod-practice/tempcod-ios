
import UIKit

public extension UIColor {
    static let customBlack = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let customBlue = #colorLiteral(red: 0.3607843137, green: 0.6470588235, blue: 0.9803921569, alpha: 1)
    static let customGray = #colorLiteral(red: 0.6745098039, green: 0.6745098039, blue: 0.6745098039, alpha: 1)
    static let customLightGray = #colorLiteral(red: 0.8901960784, green: 0.8980392157, blue: 0.9058823529, alpha: 1)
    static let customWhite = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let customYellow = #colorLiteral(red: 1, green: 0.7568627451, blue: 0.09019607843, alpha: 1)
    static let customRed = #colorLiteral(red: 1, green: 0.07058823529, blue: 0.07058823529, alpha: 1)
    static let customGreen = #colorLiteral(red: 0.3137254902, green: 0.8901960784, blue: 0.7607843137, alpha: 1)
    static let customStormBlue = #colorLiteral(red: 0.3960784314, green: 0.6549019608, blue: 0.8745098039, alpha: 1)
}
