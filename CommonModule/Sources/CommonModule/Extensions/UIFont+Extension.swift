
import UIKit

public extension UIFont {
    static let label = UIFont(name: "Avenir-Book", size: 16) ?? UIFont.systemFont(ofSize: 16)
    static let placeholder = UIFont(name: "Roboto-Regular", size: 15) ?? UIFont.systemFont(ofSize: 15)
}
