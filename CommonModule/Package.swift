// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "CommonModule",
    platforms: [.iOS(.v15)],
    products: [
        .library(
            name: "CommonModule",
            targets: ["CommonModule"]),
    ],
    dependencies: [
        .package(url: "https://github.com/SnapKit/SnapKit.git", branch: "develop")
    ],
    targets: [
        .target(
            name: "CommonModule",
            dependencies: ["SnapKit"]),
        .testTarget(
            name: "CommonModuleTests",
            dependencies: ["CommonModule"]),
    ]
)
