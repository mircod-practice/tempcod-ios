
import UIKit
import Charts
import CommonModule
import UserRepositoryModule
import SnapKit

protocol TemperatureGraphViewDelegate: AnyObject {
    func updateGraph()
}

public class TemperatureGraphView: UIView {
    // MARK: - Properties
    
    weak var delegate: TemperatureGraphViewDelegate?
    
    private var lineChart: LineChartView = {
        let chart = LineChartView()
        chart.rightAxis.enabled = false
        
        let yAxis = chart.leftAxis
        yAxis.labelFont = .label
        yAxis.labelTextColor = .customGray
        yAxis.axisLineColor = .customLightGray
        
        chart.xAxis.labelPosition = .bottom
        chart.xAxis.labelFont = .label
        chart.xAxis.labelTextColor = .customGray
        
        return chart
    }()
    
    private var lastTappedButtonIsLeft = Bool()
    
    private var entries = [ChartDataEntry]()
    
    private var graphDays: [Int] = [0,0,0,0,0]
    
    private var graphMonth = String()
    
    private var measurementDC: [DateComponents] = []
    
    private var firstIndex = 0
    
    private var lastIndex = 0

    // MARK: - UI Components
    
    private lazy var dateLabel: UILabel = {
        let label = CustomLabel().configureLabel(text: "", fontSize: 14)
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var leftButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "chevron.left"), for: .normal)
        button.tintColor = .customGray
        return button
    }()
    
    private lazy var rightButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "chevron.right"), for: .normal)
        button.tintColor = .customGray
        return button
    }()
    
    private lazy var markerView = CustomMarkerView()
    
    // MARK: - Init
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public
    
    public func setDataForView(data: [TemperatureMeasurement]) {
        getArrayOfDateComponents(data: data)
        guard !measurementDC.isEmpty else { return dateLabel.text = "no data".uppercased() }
        getDataForGraph(data: data)
        configureGraphData()
        configureLabelText()
    }
    
    // MARK: - Private
    
    private func getDataForGraph(data: [TemperatureMeasurement]) {
        entries = []
        var tempMeasurements: Double = 0
        var numberOfMeasurmenets: Double = 0
        var tempMeasurementDay: Int = 0
        var month = Int()
        
        configureGraphDates()
        
        for measurement in data {
            let currentTempMeasurementDay = getDayFromString(dateString: measurement.date ?? "")
            
            if graphDays.contains(currentTempMeasurementDay) {
                if entries.isEmpty {
                    month = getMonthFromString(dateString: measurement.date ?? "error")
                    getMonthFromDate(dateString: measurement.date ?? "error")
                }
                
                if currentTempMeasurementDay != tempMeasurementDay && tempMeasurementDay != 0 {
                    tempMeasurements = tempMeasurements / numberOfMeasurmenets
                    entries.append(ChartDataEntry(x: Double(tempMeasurementDay), y: tempMeasurements))
                    tempMeasurements = 0
                    numberOfMeasurmenets = 0
                }
                
                tempMeasurements = tempMeasurements + Double(measurement.temperature ?? -100)
                numberOfMeasurmenets = numberOfMeasurmenets + 1
                tempMeasurementDay = currentTempMeasurementDay
            }
        }
        tempMeasurements = tempMeasurements / numberOfMeasurmenets
        entries.append(ChartDataEntry(x: Double(tempMeasurementDay), y: tempMeasurements))
        
        checkForDataEdges(monthInt: month)
    }
    
    private func getDayFromString(dateString: String) -> Int {
        var dayString = String()
        dayString.append(dateString[dateString.index(dateString.startIndex, offsetBy: 8)])
        dayString.append(dateString[dateString.index(dateString.startIndex, offsetBy: 9)])
        let day = Int(dayString) ?? -100
        return day
    }
    
    private func getMonthFromString(dateString: String) -> Int {
        var monthString = String()
        monthString.append(dateString[dateString.index(dateString.startIndex, offsetBy: 5)])
        monthString.append(dateString[dateString.index(dateString.startIndex, offsetBy: 6)])
        let month = Int(monthString) ?? -100
        return month
    }
    
    private func configureGraphDates() {
        graphDays = []
        var i = 4
        
        if firstIndex == 0 {
            while i >= 0 {
                if measurementDC.count >= 5 {
                    graphDays.append(measurementDC[measurementDC.count - 1 - i].day ?? 0)
                } else {
                    graphDays.append(contentsOf: measurementDC.compactMap {$0.day})
                    break
                }
                i = i - 1
            }
        } else if lastTappedButtonIsLeft {
            while i >= 0 {
                if firstIndex - i >= 0 {
                    graphDays.append(measurementDC[firstIndex - i].day ?? 0)
                }
                i = i - 1
            }
        } else {
            for index in 0...4 {
                graphDays.append(measurementDC[lastIndex + index].day ?? 0)
            }
        }
        
        firstIndex = measurementDC.firstIndex(where: { $0.day == graphDays[0] }) ?? -100
        lastIndex = measurementDC.firstIndex(where: { $0.day == graphDays[graphDays.count - 1] }) ?? -100
    }
    
    private func checkForDataEdges(monthInt: Int) {
        if graphDays[graphDays.count - 1] == measurementDC[measurementDC.count - 1].day && monthInt == measurementDC[0].month {
            rightButton.isHidden = true
        } else {
            rightButton.isHidden = false
        }
        
        if graphDays[0] == measurementDC[0].day && monthInt == measurementDC[0].month {
            leftButton.isHidden = true
        } else {
            leftButton.isHidden = false
        }
    }
    
    private func getArrayOfDateComponents(data: [TemperatureMeasurement]) {
        let calendar = Calendar.current
        var currentMeasureDate = DateComponents()
        for measurmenet in data {
            currentMeasureDate = getDateComponentsFromString(date: measurmenet.date ?? "error.type", calendar: calendar)
            if !measurementDC.contains(currentMeasureDate) {
                measurementDC.append(currentMeasureDate)
            }
        }
    }
    
    private func getDateComponentsFromString(date: String, calendar: Calendar) -> DateComponents {
        let date = getDateFromString(dateString: date)
        let dateComponents = calendar.dateComponents([.day , .month], from: date)
        return dateComponents
    }
    
    private func getDateFromString(dateString: String) -> Date {
        let dateFormatter = ISO8601DateFormatter()
        dateFormatter.formatOptions = [.withInternetDateTime]
        guard let date = dateFormatter.date(from: dateString) else { return Date.init() }
        return date
    }
    
    private func getMonthFromDate(dateString: String) {
           let dateFormatter = ISO8601DateFormatter()
           dateFormatter.formatOptions = [.withInternetDateTime]
           let date = dateFormatter.date(from: dateString)
           let stringFormatter = DateFormatter()
           stringFormatter.dateFormat = "MMMM"
           stringFormatter.locale = NSLocale(localeIdentifier: "en_EN_POSIX") as Locale
           graphMonth = stringFormatter.string(from: date ?? Date.init())
    }
    
    private func configureGraphData() {
        let set = LineChartDataSet(entries: entries)
        set.mode = .horizontalBezier
        set.setColor(.customBlue)
        set.circleColors = [NSUIColor.customStormBlue.withAlphaComponent(0.5)]
        set.circleRadius = 10
        set.circleHoleColor = .customBlue
        set.setDrawHighlightIndicators(false)
        
        let data = LineChartData(dataSet: set)
        data.setDrawValues(false)
        lineChart.data = data
    }
    
    private func configureLabelText() {
        var text = graphMonth.uppercased() + "\n" + String(graphDays[0])
        if graphDays.count != 1 {
            text = graphMonth.uppercased() + "\n" + String(graphDays[0]) + " - " + String(graphDays[graphDays.count - 1])
        }
        dateLabel.text = text
    }

    private func configureView() {
        addViews()
        configureGraph()
        configureMarkerView()
        configureButtons()
        configureLayouts()
    }
    
    private func addViews() {
        addSubview(dateLabel)
        addSubview(leftButton)
        addSubview(rightButton)
        addSubview(lineChart)
    }
    
    private func configureGraph() {
        lineChart.delegate = self
        
        lineChart.doubleTapToZoomEnabled = false
        lineChart.legend.enabled = false
        lineChart.leftAxis.labelXOffset = -6
        lineChart.leftAxis.drawGridLinesEnabled = false
        lineChart.leftAxis.granularity = 1
        lineChart.xAxis.gridColor = .customLightGray
        lineChart.xAxis.drawAxisLineEnabled = false
        lineChart.xAxis.granularity = 1
        
        markerView.chartView = lineChart
        lineChart.marker = markerView
    }
    
    private func configureMarkerView() {
        markerView.offset = CGPoint(x: -(self.frame.width/2) - 20, y: -self.frame.height - 65)
    }
    
    private func configureButtons() {
        leftButton.addTarget(self, action: #selector(leftButtonTapped), for: .touchUpInside)
        rightButton.addTarget(self, action: #selector(rightButtonTapped), for: .touchUpInside)
    }
    
    private func configureLayouts() {
        leftButton.snp.makeConstraints { make in
            make.left.equalTo(self.snp.left).inset(25)
            make.right.equalTo(self.snp.right).inset(320)
            make.top.equalTo(self.snp.top).inset(46)
        }
        rightButton.snp.makeConstraints { make in
            make.right.equalTo(self.snp.right).inset(25)
            make.left.equalTo(self.snp.left).inset(320)
            make.top.equalTo(self.snp.top).inset(46)
        }
        dateLabel.snp.makeConstraints { make in
            make.top.equalTo(self.bounds.size).inset(40)
            make.left.equalTo(leftButton.snp.right)
            make.right.equalTo(rightButton.snp.left)
        }
        lineChart.snp.makeConstraints { make in
            make.bottom.left.right.equalTo(self.bounds.size).inset(20)
            make.top.equalTo(self.bounds.size).inset(80)
        }
    }
    
    // MARK: - Objc
    
    @objc func leftButtonTapped() {
        lastTappedButtonIsLeft = true
        delegate?.updateGraph()
    }
    
    @objc func rightButtonTapped() {
        lastTappedButtonIsLeft = false
        delegate?.updateGraph()
    }
}
// MARK: - Delegate

extension TemperatureGraphView: ChartViewDelegate {
    public func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        guard let dataSet = chartView.data?.dataSets[highlight.dataSetIndex] else { return }
        let entryIndex = dataSet.entryIndex(entry: entry)

        markerView.label.text = String (format: "%.1f", entries[entryIndex].y)
    }
}
