
import UIKit
import CommonModule
import Charts

public class CustomMarkerView: MarkerView {
    // MARK: - UIComponents
    
    private lazy var imageView = UIImageView(image: UIImage(named: "markerView"))

    lazy var label: UILabel = {
        let label = UILabel()
        label.textColor = .customWhite
        label.font = .label
        return label
    }()
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // MARK: - Private
    
    private func configureView () {
        addSubview(imageView)
        addSubview(label)
    
        self.frame = CGRect(x: 0, y: 0, width: 41.5, height: 50)
        label.frame = CGRect(x: 4, y: 10, width: 32, height: 30)
        imageView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
    }
}

