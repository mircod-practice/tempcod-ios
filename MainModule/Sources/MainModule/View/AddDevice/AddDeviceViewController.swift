
import UIKit
import CoreBluetooth
import CommonModule

public class AddDeviceViewController: UIViewController {
    
    private lazy var titleButton: UIButton = CustomButton().configureBackButton(title: "Device list")
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "\(UITableViewCell.self)")
        return tableView
    }()
    private var peripherals: [CBPeripheral] = BluetoothManager.shared.peripheralsFound

    public override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        tableView.reloadData()
    }
    
    private func configureView() {
        view = CustomBackground().configureCustomBackground(isGearshapeNeeded: false, view: self.view)
        addViews()
        configureTableView()
        configureBackButton()
        configureLayouts()
        configureBluetooth()
    }
    
    private func addViews() {
        view.addSubview(titleButton)
        view.addSubview(tableView)
    }
    
    private func configureTableView() { 
        tableView.backgroundColor = .clear
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 13, bottom: 0, right: 13)
        tableView.rowHeight = 60
    }
    
    private func configureBackButton() {
        titleButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
    }
    
    private func configureLayouts() {
        titleButton.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalToSuperview().inset(20)
        }
        tableView.snp.makeConstraints { make in
            make.top.equalTo(titleButton.snp.bottom).offset(10)
            make.left.right.equalToSuperview().inset(18)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
        }
    }
    
    private func configureBluetooth() {
        BluetoothManager.shared.startScan()
        BluetoothManager.shared.addPeripheralClosure = { [weak self] peripheral in
            self?.peripherals.append(peripheral)
            self?.tableView.reloadData()
        }
    }
    private func configureCell(cell: UITableViewCell, row: Int) -> UITableViewCell {
        var content = cell.defaultContentConfiguration()
        let name = peripherals[row].name ?? ""
        content.text = "Device name: \(name)"
        content.textProperties.font = .label
        cell.backgroundColor = .clear
        cell.contentConfiguration = content
        return cell
    }
    
    @objc private func backButtonAction() {
        navigationController?.popViewController(animated: true)
    }
}

extension AddDeviceViewController: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        peripherals.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        return configureCell(cell: cell, row: indexPath.row)
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        BluetoothManager.shared.connectPeripheral(name: peripherals[indexPath.row].name ?? "")
    }
}
