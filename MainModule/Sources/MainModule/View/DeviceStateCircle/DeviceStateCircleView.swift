
import UIKit

class DeviceStateCircleView: UIView {
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Draw
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            print("could not get graphics context")
            return
        }
        context.setLineWidth(5)
        context.setFillColor(UIColor(red: 0.957, green: 0.941, blue: 0.941, alpha: 11).cgColor)
        context.addEllipse(in: CGRect(x: 31.5, y: 31.5, width: 205, height: 205))
        context.drawPath(using: .fill)
        
        context.beginPath()
        context.setLineWidth(30)
        context.setFillColor(UIColor.clear.cgColor)
        context.setStrokeColor(UIColor(red: 0.91, green: 0.91, blue: 0.914, alpha: 1).cgColor)
        context.addArc(center: CGPoint(x:134, y: 134), radius: 117, startAngle: 2.25, endAngle: 0.9, clockwise: false)
        context.drawPath(using: .fillStroke)
    }
}
