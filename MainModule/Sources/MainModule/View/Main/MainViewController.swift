import UIKit
import CommonModule
import SnapKit
import UserRepositoryModule

public class MainViewController: UIViewController {
    
    // MARK: - UI Components
    
    private lazy var circleView: DeviceStateCircleView = {
        let view = DeviceStateCircleView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var gradientLayer = CAGradientLayer()
    private lazy var titleLabel: UILabel = CustomLabel().configureLabel(text: "Home", fontSize: 26)
    private lazy var tempUnitLabel: UILabel = CustomLabel().configureBlueLabel(username: "Celsius")
    private lazy var batteryImage = UIImageView()
    private lazy var temperatureLabel: UILabel = CustomLabel().configureLabel(text: "25", fontSize: 25)
    private lazy var addDeviceButton: UIButton = CustomButton().configureColoredButton(title: "Add device", color: .customBlue, sideInset: 50)
    private lazy var resetTemperatureButton: UIButton = UIButton()
    
    private lazy var graphView: TemperatureGraphView = {
        let view = TemperatureGraphView()
        view.layer.cornerRadius = 45
        view.layer.backgroundColor = UIColor.customWhite.cgColor
        view.delegate = self
        return view
    }()
    
    // MARK: - Properties
    
    private var isCelsius: Bool = true
    
    private var measurements: [TemperatureMeasurement]?

    private var battery: Int?
    
    // MARK: - View Model
    
    var viewModel: MainViewModel! {
        didSet {
            self.viewModel.temperatureDidChange = { [weak self] viewModel in
                self?.temperatureLabel.text = "\(viewModel.temperature)"
                if self?.isCelsius == true {
                    if viewModel.temperature < 36 {
                        self?.updateGradientColor(color: .customStormBlue)
                    } else if (viewModel.temperature >= 37) {
                        self?.updateGradientColor(color: .customRed)
                    } else {
                        self?.updateGradientColor(color: .customGreen)
                    }
                } else {
                    if viewModel.temperature < 96.8 {
                        self?.updateGradientColor(color: .customStormBlue)
                    } else if (viewModel.temperature >= 98.6) {
                        self?.updateGradientColor(color: .customRed)
                    } else {
                        self?.updateGradientColor(color: .customGreen)
                    }
                }
            }
            self.viewModel.batteryDidChange = { [weak self] viewModel in
                self?.battery = viewModel.battery
                if viewModel.battery ?? 100 <= 20 {
                    self?.batteryImage.image = UIImage(named: "lowBattery")
                } else if viewModel.battery ?? 100 < 80 {
                    self?.batteryImage.image = UIImage(named: "middleBattery")
                } else {
                    self?.batteryImage.image = UIImage(named: "fullBattery")
                }
            }
            self.viewModel.isConnectedDidChange = { [weak self] viewModel in
                if viewModel.isConnected {
                    self?.temperatureLabel.isHidden = false
                    self?.resetTemperatureButton.isHidden = false
                    self?.addDeviceButton.isHidden = true
                    self?.gradientLayer.isHidden = false
                    self?.circleView.isHidden = false
                    self?.tempUnitLabel.isHidden = false
                    self?.batteryImage.isHidden = false
                } else {
                    self?.temperatureLabel.isHidden = true
                    self?.resetTemperatureButton.isHidden = true
                    self?.addDeviceButton.isHidden = false
                    self?.gradientLayer.isHidden = true
                    self?.circleView.isHidden = true
                    self?.tempUnitLabel.isHidden = true
                    self?.batteryImage.isHidden = true
                }
            }
            self.viewModel.isCelsiusDidChange = { [weak self] viewModel in
                self?.isCelsius = viewModel.isCelsius
                if viewModel.isCelsius {
                    self?.tempUnitLabel.text = "Celsius"
                } else {
                    self?.tempUnitLabel.text = "Fahrenheit"
                }
            }
            self.viewModel.measurementsDidChange = { [weak self] viewModel in
                self?.measurements = viewModel.measurements
                self?.updateGraph()
            }
        }
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView() {
        view = CustomBackground().configureCustomBackground(isGearshapeNeeded: false, view: self.view)
        addViews()
        configureButton()
        configureLayouts()
        configureGradientLayer()
    }
    
    private func addViews() {
        view.addSubview(titleLabel)
        view.addSubview(resetTemperatureButton)
        view.addSubview(addDeviceButton)
        view.layer.addSublayer(gradientLayer)
        view.addSubview(circleView)
        view.addSubview(tempUnitLabel)
        view.addSubview(temperatureLabel)
        view.addSubview(batteryImage)
        view.addSubview(graphView)
    }
    
    private func configureButton() {
        resetTemperatureButton.setImage(UIImage(named: "bluetooth image"), for: .normal)
        resetTemperatureButton.addTarget(self, action: #selector(resetTemperatureAction), for: .touchUpInside)
        addDeviceButton.addTarget(self, action: #selector(addDeviceAction), for: .touchUpInside)
    }
    
    private func configureGradientLayer() {
        gradientLayer.type = .radial
        gradientLayer.locations = [0.77]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.frame = CGRect(x: 63, y: 99, width: 265, height: 265)
        gradientLayer.cornerRadius = 130
        gradientLayer.colors = [UIColor.customGray.cgColor, UIColor.customWhite.withAlphaComponent(0).cgColor]
        gradientLayer.shouldRasterize = true
    }
    
    private func updateGradientColor(color: UIColor) {
        gradientLayer.colors = [color.cgColor, UIColor.customWhite.withAlphaComponent(0).cgColor]
    }
    
    private func configureLayouts() {
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalToSuperview().inset(30)
        }
        resetTemperatureButton.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(8)
            make.right.equalToSuperview().inset(18)
        }
        temperatureLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(165)
            make.left.right.equalToSuperview().inset(48)
        }
        tempUnitLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(130)
            make.centerX.equalTo(view.snp.centerX)
        }
        batteryImage.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(210)
            make.centerX.equalTo(view.snp.centerX)
        }
        circleView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(50)
            make.centerX.equalTo(view.snp.centerX)
            make.height.width.equalTo(266)
        }
        addDeviceButton.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(115)
            make.left.right.equalToSuperview().inset(48)
        }
        graphView.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview().inset(10)
            make.height.equalTo(350)
        }
    }
    
    @objc private func resetTemperatureAction() {
        temperatureLabel.text = "25"
        viewModel.didTapResetTemperatureButton()
    }
    
    @objc private func addDeviceAction() {
        viewModel.didTapAddDeviceButton()
    }
}

extension MainViewController: TemperatureGraphViewDelegate {
    func updateGraph() {
        graphView.setDataForView(data: measurements ?? [])
    }
}
