
import Foundation
import Alamofire
import UserRepositoryModule
import CommonModule
import SwiftyJSON
import SettingsModule

public enum NextRoute {
    case addDevice
}

protocol MainViewModelProtocol {
    var temperature: Double { get }
    var battery: Int? { get }
    var isConnected: Bool { get }
    var isCelsius: Bool { get }
    var measurements: [TemperatureMeasurement] { get }
    var temperatureDidChange: ((MainViewModelProtocol) -> Void)? { get set }
    var batteryDidChange: ((MainViewModelProtocol) -> Void)? { get set }
    var isConnectedDidChange: ((MainViewModelProtocol) -> Void)? { get set }
    var isCelsiusDidChange: ((MainViewModelProtocol) -> Void)? { get set }
    var measurementsDidChange: ((MainViewModelProtocol) -> Void)? { get set }
    var completionHandler: ((NextRoute) -> Void)? { get set }
    func didTapAddDeviceButton()
    func didTapResetTemperatureButton()
}

public class MainViewModel: MainViewModelProtocol {
    
    // MARK: - Properties
    
    var temperature: Double = 25 {
        didSet {
            self.temperatureDidChange?(self)
        }
    }
    var battery: Int? {
        didSet {
            self.batteryDidChange?(self)
        }
    }
    var isConnected: Bool = true {
        didSet {
            self.isConnectedDidChange?(self)
        }
    }
    var isCelsius: Bool = true {
        didSet {
            self.isCelsiusDidChange?(self)
        }
    }
    var measurements: [TemperatureMeasurement] = [] {
        didSet {
            self.measurementsDidChange?(self)
        }
    }
    
    var temperatureDidChange: ((MainViewModelProtocol) -> Void)?
    var batteryDidChange: ((MainViewModelProtocol) -> Void)?
    var isConnectedDidChange: ((MainViewModelProtocol) -> Void)?
    var isCelsiusDidChange: ((MainViewModelProtocol) -> Void)?
    var measurementsDidChange: ((MainViewModelProtocol) -> Void)?
    
    var completionHandler: ((NextRoute) -> Void)?
    
    private var needSend: Bool = false

    // MARK: - Init
    
    public init() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) { [weak self] in
            self?.getUser()
            self?.isConnected = false
            TemperatureUnitService.shared.isCelsiusClosure = { [weak self] boolValue in
                if self?.isCelsius != boolValue {
                    self?.isCelsius = boolValue
                    guard let measurements = self?.measurements else { return }
                    if boolValue {
                        self?.temperature = TemperatureUnitService.shared.getCelsius(fahrenheit: self?.temperature ?? 0)
                        self?.measurementsToCelsius(measurements: measurements)
                    } else {
                        self?.temperature = TemperatureUnitService.shared.getFahrenheit(celsius: self?.temperature ?? 0)
                        self?.measurementsToFahrenheit(measurements: measurements)
                    }
                }
            }
            BluetoothManager.shared.isConnectedClosure = { [weak self] boolValue in
                self?.isConnected = boolValue
                if boolValue {
                    BluetoothManager.shared.peripheralManager?.batteryClosure = { [weak self] battery in
                        self?.battery = battery
                    }
                    BluetoothManager.shared.peripheralManager?.temperatureClosure = { [weak self] temperature in
                        var newTemperature = temperature
                        if self?.isCelsius == false {
                            newTemperature = TemperatureUnitService.shared.getFahrenheit(celsius: temperature)
                        }
                        if self?.temperature ?? 0 < newTemperature {
                            self?.temperature = newTemperature
                            self?.needSend = true
                        } else if self?.needSend ?? false {
                            self?.sendTemperature()
                            self?.needSend = false
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Public
    
    public func didTapAddDeviceButton() {
        completionHandler?(.addDevice)
    }
    
    public func didTapResetTemperatureButton() {
        if isCelsius {
            temperature = 25
        } else {
            temperature = 77
        }
        needSend = true
    }
    
    // MARK: - Private
    
    private func measurementsToFahrenheit(measurements: [TemperatureMeasurement]) {
        var result: [TemperatureMeasurement] = []
        for measurement in measurements {
            guard let temperature = measurement.temperature else { return }
            result.append(TemperatureMeasurement(temperature: TemperatureUnitService.shared.getFahrenheit(celsius: temperature), date: measurement.date))
        }
        self.measurements = result
    }
    
    private func measurementsToCelsius(measurements: [TemperatureMeasurement]) {
        var result: [TemperatureMeasurement] = []
        for measurement in measurements {
            guard let temperature = measurement.temperature else { return }
            result.append(TemperatureMeasurement(temperature: TemperatureUnitService.shared.getCelsius(fahrenheit: temperature), date: measurement.date))
        }
        self.measurements = result
    }
    
    private func getUser() {
        guard let getUserUrl = URL(string: CustomPath.absolutePath.rawValue + CustomPath.getCurrentUserPath.rawValue) else { return }
        guard let accessToken = UserRepistory.shared.getAccessToken() else { return }
        AF.request(getUserUrl, method: .get, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Bearer \(accessToken)")])).response { [weak self] response in
            switch response.result {
            case .success(let value):
                var result: [TemperatureMeasurement] = []
                let json = JSON(value!)
                print(json)
                let username = json["username"].stringValue
                let firstname = json["first_name"].stringValue
                let lastname = json["last_name"].stringValue
                for measurement in json["measurements"] {
                    result.append(TemperatureMeasurement(temperature: measurement.1["temperature"].doubleValue, date: measurement.1["measured_at"].stringValue))
                }
                self?.measurements = result.reversed()
                UserRepistory.shared.saveUser(user: User(username: username, firstname: firstname, lastname: lastname))
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func sendTemperature() {
        guard let url = URL(string: CustomPath.absolutePath.rawValue +  CustomPath.sendTempPath.rawValue) else { return }
        var params: [String: Any] = [:]
        if isCelsius {
            params = [
                "temperature": temperature,
                "measured_at": Date().description
            ]
        } else {
            params = [
                "temperature": TemperatureUnitService.shared.getCelsius(fahrenheit: temperature),
                "measured_at": Date().description
            ]
        }
        guard let accessToken = UserRepistory.shared.getAccessToken() else { return }
        AF.request(url, method: .post, parameters: params, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Bearer \(accessToken)")])).response { response in
            switch response.result {
            case .success(_):
                print(response.response?.statusCode ?? 0)
            case .failure(let error):
                print(error)
            }
        }
    }
}
