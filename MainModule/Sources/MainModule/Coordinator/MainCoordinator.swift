
import UIKit
import CoordinatorAbstractModule
import CommonModule

public class MainCoordinator: CoordinatorAbstract {
    public var navigationController: UINavigationController
    public var coordinators: [CoordinatorAbstract] = []
        
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    public func start() {
        BluetoothManager.shared.showResultClosure = { [weak self] result in
            switch result {
            case .successful:
                self?.showSuccessfulScreen()
            case .fail:
                self?.showFailScreen()
            }
        }
        let viewModel = MainViewModel()
        let viewController = MainViewController()
        viewController.viewModel = viewModel
        viewModel.isConnected = false
        viewModel.completionHandler = { route in
            switch route {
            case .addDevice:
                self.showAddDevice()
            }
        }
        
        navigationController.show(viewController, sender: self)
    }
    
    private func showAddDevice() {
        let addDeviceViewController = AddDeviceViewController()
        navigationController.show(addDeviceViewController, sender: self)
    }
    
    private func showSuccessfulScreen() {
        guard let deviceName = BluetoothManager.shared.peripheralManager?.cbPeripheral.name else { return }
        let vc = CustomViewController()
        vc.dialogStateImageView.image = UIImage(named: "success")
        vc.largeLabel.text = "Successfully connected to \(deviceName)"
        vc.largeLabel.numberOfLines = 2
        vc.smallLabel.isHidden = false
        vc.coloredButton.setTitle("Ok", for: .normal)
        vc.simpleButton.isHidden = false
        vc.simpleButton.layer.isHidden = true
        vc.modalPresentationStyle = .overCurrentContext
        navigationController.present(vc, animated: true)
        vc.coloredButton.addTarget(self, action: #selector(dismissViewController), for: .touchUpInside)
    }
    
    private func showFailScreen() {
        guard let deviceName = BluetoothManager.shared.peripheralManager?.cbPeripheral.name else { return }
        let vc = CustomViewController()
        vc.dialogStateImageView.image = UIImage(named: "success")
        vc.largeLabel.text = "Faild to connect to \(deviceName)"
        vc.smallLabel.isHidden = false
        vc.coloredButton.setTitle("Retry", for: .normal)
        vc.simpleButton.setTitle("Cancel connection", for: .normal)
        vc.modalPresentationStyle = .overCurrentContext
        navigationController.present(vc, animated: true)
        vc.coloredButton.addTarget(self, action: #selector(dismissViewController), for: .touchUpInside)
        vc.simpleButton.addTarget(self, action: #selector(retryButtonAction), for: .touchUpInside)
    }
    
    @objc private func dismissViewController() {
        navigationController.popViewController(animated: false)
        navigationController.dismiss(animated: true)
    }
    
    @objc private func retryButtonAction() {
        guard let name = BluetoothManager.shared.peripheralManager?.cbPeripheral.name else { return }
        BluetoothManager.shared.connectPeripheral(name: name)
    }
}
