
import Foundation

public class DataParser {
    public func getTemperature(_ value: Data) -> Double {
        let temperatureBytes = [UInt8](value.subdata(in: 0..<2))
        let temperature = temperatureBytes.reversed().withUnsafeBytes { $0.load(as: UInt16.self) }
        print(Double(temperature)/100)
        return Double(temperature)/100
    }
}
