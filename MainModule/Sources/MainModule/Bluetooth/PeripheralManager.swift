
import Foundation
import CoreBluetooth
import Alamofire
import UserRepositoryModule
import CommonModule

public enum CharacheristicUUID: String {
    case temperature = "FFED"
    case battery = "2A19"
}

public class PeripheralManager: NSObject {
    
    // MARK: - Properties
    
    let cbPeripheral: CBPeripheral

    let dataParser = DataParser()
    
    var temperatureClosure: ((Double) -> Void)?
    var batteryClosure: ((Int) -> Void)?
    
    // MARK: - Init
    
    init(peripheral: CBPeripheral) {
        self.cbPeripheral = peripheral
        
        super.init()
        peripheral.delegate = self
    }
    
    // MARK: - Private
    
    private func sendData(battery: Int) {
        guard let url = URL(string: CustomPath.absolutePath.rawValue +  CustomPath.connectDevicePath.rawValue) else { return }
        let params: [String: Any] = [
            "uuid": cbPeripheral.name ?? "",
            "charge": battery
        ]
        guard let accessToken = UserRepistory.shared.getAccessToken() else { return }
        AF.request(url, method: .post, parameters: params, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Bearer \(accessToken)")])).response { response in
            switch response.result {
            case .success(_):
                print(response.response?.statusCode ?? 0)
            case .failure(let error):
                print(error)
            }
        }
    }
}

// MARK: - CBPeripheralDelegate

extension PeripheralManager: CBPeripheralDelegate {
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard let characteristics = service.characteristics else { return }
        print(characteristics)
        for characteristic in characteristics {
            if characteristic.properties.contains(.read) {
                peripheral.readValue(for: characteristic)
            }
            if characteristic.properties.contains(.notify) {
                peripheral.setNotifyValue(true, for: characteristic)
            }
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services ?? [] {
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        guard let value = characteristic.value else { return }
        let stringFromData = String(data: value, encoding: .utf8)
        print(stringFromData ?? "")
        switch characteristic.uuid {
        case CBUUID(string: CharacheristicUUID.temperature.rawValue):
            temperatureClosure?(dataParser.getTemperature(value))
        case CBUUID(string: CharacheristicUUID.battery.rawValue):
            let battery = Int([UInt8](value)[0])
            sendData(battery: battery)
            batteryClosure?(battery)
        default:
            print(" \(characteristic.uuid.uuidString): \([UInt8](value))")
        }
    }
}
