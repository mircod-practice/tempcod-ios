
import Foundation
import CoreBluetooth

public enum ResultConnection {
    case successful
    case fail
}

public enum ServiceUUID: String {
    case scanServices = "D03A"
    case battery = "180F"
    case temperature = "FFED"
}

public class BluetoothManager: NSObject {
    
    // MARK: - Properties
    
    static let shared = BluetoothManager()
    
    let centralManager = CBCentralManager()
    var peripheralManager: PeripheralManager?
    let scanOptions: [String: Any] = [CBCentralManagerScanOptionAllowDuplicatesKey: true]
    let scanServices: [CBUUID] = [CBUUID(string: ServiceUUID.scanServices.rawValue)]
    var peripheralsFound: [CBPeripheral] = []
    var isBluetoothOn = false
    var addPeripheralClosure: ((CBPeripheral) -> Void)?
    var isConnectedClosure: ((Bool) -> Void)?
    var showResultClosure: ((ResultConnection) -> Void)?
    
    // MARK: - Init
    
    override init() {
        super.init()
        centralManager.delegate = self
    }
    
    // MARK: - Public
    
    public func startScan() {
        if isBluetoothOn {
            centralManager.scanForPeripherals(withServices: scanServices, options: scanOptions)
        }
    }
    
    public func connectPeripheral(name: String) {
        for peripheral in peripheralsFound {
            if peripheral.name == name {
                centralManager.connect(peripheral)
                peripheralManager = PeripheralManager(peripheral: peripheral)
            }
        }
    }
}

// MARK: - CBCentralManagerDelegate

extension BluetoothManager: CBCentralManagerDelegate {
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown, .resetting, .unsupported, .unauthorized, .poweredOff:
            isBluetoothOn = false
        case .poweredOn:
            isBluetoothOn = true
        @unknown default:
            break
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        guard let _ = peripheral.name else { return }
        
        if !peripheralsFound.contains(peripheral) {
            peripheralsFound.append(peripheral)
            addPeripheralClosure?(peripheral)
        }
//        print("\(peripheral.name) : \(peripheral.identifier.uuidString)")
    }
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connection was successful")
        centralManager.stopScan()
        isConnectedClosure?(true)
        showResultClosure?(.successful)
        peripheral.discoverServices(nil)
        peripheralsFound = []
    }
    
    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Fail to connect", error.debugDescription)
        isConnectedClosure?(false)
        showResultClosure?(.fail)
    }
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("Disconnect")
        isConnectedClosure?(false)
        peripheralsFound = []
        startScan()
    }
}

