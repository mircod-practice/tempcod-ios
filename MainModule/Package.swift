// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MainModule",
    platforms: [.iOS(.v15)],
    products: [
        .library(
            name: "MainModule",
            targets: ["MainModule"]),
    ],
    dependencies: [
        .package(path: "../CommonModule"),
        .package(path: "../CoordinatorAbstractModule"),
        .package(url: "https://github.com/SnapKit/SnapKit.git", branch: "develop"),
        .package(url: "https://github.com/Alamofire/Alamofire.git", .upToNextMajor(from: "5.6.1")),
        .package(url: "https://github.com/danielgindi/Charts.git", .upToNextMajor(from: "4.0.3")),
        .package(path: "../UserRepositoryModule"),
        .package(path: "../SettingsModule")
    ],
    targets: [
        .target(
            name: "MainModule",
            dependencies: ["CommonModule", "CoordinatorAbstractModule" , "SnapKit", "Alamofire", "Charts", "UserRepositoryModule", "SettingsModule"]),
        .testTarget(
            name: "MainModuleTests",
            dependencies: ["MainModule"]),
    ]
)
