// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "TabBarModule",
    platforms: [.iOS(.v15)],
    products: [
        .library(
            name: "TabBarModule",
            targets: ["TabBarModule"]),
    ],
    dependencies: [
        .package(path: "../CoordinatorAbstractModule"),
        .package(path: "../MainModule"),
        .package(path: "../SettingsModule"),
        .package(path: "../ProfileModule")
    ],
    targets: [
        .target(
            name: "TabBarModule",
            dependencies: ["CoordinatorAbstractModule", "MainModule", "SettingsModule", "ProfileModule"]),
        .testTarget(
            name: "TabBarModuleTests",
            dependencies: ["TabBarModule"]),
    ]
)
