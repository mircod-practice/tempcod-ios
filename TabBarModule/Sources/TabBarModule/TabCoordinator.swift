
import UIKit
import CoordinatorAbstractModule
import MainModule
import SettingsModule
import ProfileModule

public class TabCoordinator: NSObject, CoordinatorAbstract {
    
    // MARK: - Properties
    
    public var navigationController: UINavigationController
    public var coordinators: [CoordinatorAbstract] = []
    public var tabBarController: UITabBarController
    
    public var tabBarFlowCompletionHandler: (() -> Void)?
    
    // MARK: - Init
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.tabBarController = .init()
    }
    
    // MARK: - Public
    
    public func start() {
        let pages: [TabBarPage] = [.profile, .main, .settings].sorted(by: { $0.pageOrderNumber() < $1.pageOrderNumber() })
        let controllers: [UINavigationController] = pages.map({ getTabController(page: $0) })
        
        prepareTabBarController(withTabControllers: controllers)
    }
    
    // MARK: - Private
    
    private func prepareTabBarController(withTabControllers tabControllers: [UIViewController]) {
        navigationController.navigationItem.hidesBackButton = true
        navigationController.show(tabBarController, sender: self)
        tabBarController.delegate = self
        tabBarController.tabBar.backgroundColor = .white
        tabBarController.setViewControllers(tabControllers, animated: true)
        tabBarController.selectedIndex = TabBarPage.main.pageOrderNumber()
        tabBarController.tabBar.isTranslucent = false
        navigationController.viewControllers = [tabBarController]
        navigationController.isNavigationBarHidden = true
    }
    
    private func getTabController(page: TabBarPage) -> UINavigationController {
        let navigationController = UINavigationController()
        navigationController.setNavigationBarHidden(false, animated: false)
        navigationController.tabBarItem = UITabBarItem.init(title: nil, image: page.pageImageValue(), tag: page.pageOrderNumber())
        navigationController.isNavigationBarHidden = true
        switch page {
        case .profile:
            showProfile(navigationController: navigationController)
        case .main:
            showMain(navigationController: navigationController)
        case .settings:
            showSettings(navigationController: navigationController)
        }
        return navigationController
    }
    
    private func showProfile(navigationController: UINavigationController) {
        let profileCoordinator = ProfileCoordinator(navigationController: navigationController)
        coordinators.append(profileCoordinator)
        profileCoordinator.flowCompletionHandler = { [weak self] route in
            if route == .logout {
                self?.tabBarFlowCompletionHandler?()
                self?.navigationController.isNavigationBarHidden = false
            }
        }
        
        profileCoordinator.start()
    }
    
    private func showMain(navigationController: UINavigationController) {
        let mainCoordinator = MainCoordinator(navigationController: navigationController)
        coordinators.append(mainCoordinator)
        mainCoordinator.start()
    }
    
    private func showSettings(navigationController: UINavigationController) {
        let settingsCoordinator = SettingsCoordinator(navigationController: navigationController)
        coordinators.append(settingsCoordinator)
        settingsCoordinator.start()
    }
    
    private func currentPage() -> TabBarPage? { TabBarPage.init(index: tabBarController.selectedIndex) }
    
    private func selectPage(_ page: TabBarPage) {
        tabBarController.selectedIndex = page.pageOrderNumber()
    }
    
    private func setSelectedIndex(_ index: Int) {
        guard let page = TabBarPage.init(index: index) else { return }
        
        tabBarController.selectedIndex = page.pageOrderNumber()
    }
}

// MARK: - UITabBarControllerDelegate

extension TabCoordinator: UITabBarControllerDelegate {
    public func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
    }
}
