
import Foundation
import UIKit

public enum TabBarPage {
    
    // MARK: - Cases
    
    case profile
    case main
    case settings
    
    // MARK: - Init
    
    init?(index: Int) {
        switch index {
        case 0:
            self = .profile
        case 1:
            self = .main
        case 2:
            self = .settings
        default:
            return nil
        }
    }
    
    // MARK: - Public
    
    public func pageImageValue() -> UIImage? {
        switch self {
        case .profile:
            return UIImage(named: "profile")
        case .main:
            return UIImage(named: "main")
        case .settings:
            return UIImage(named: "settings")
        }
    }
    
    public func pageTitleValue() -> String {
        switch self {
        case .profile:
            return "Profile"
        case .main:
            return "Main"
        case .settings:
            return "Settings"
        }
    }
    
    public func pageOrderNumber() -> Int {
        switch self {
        case .profile:
            return 0
        case .main:
            return 1
        case .settings:
            return 2
        }
    }
}
