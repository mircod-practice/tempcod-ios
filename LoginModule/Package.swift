// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "LoginModule",
    platforms: [.iOS(.v15)],
    products: [
        .library(
            name: "LoginModule",
            targets: ["LoginModule"]),
    ],
    dependencies: [
        .package(path: "../CommonModule"),
        .package(path: "../CoordinatorAbstractModule"),
        .package(path: "../UserRepositoryModule"),
        .package(url: "https://github.com/SnapKit/SnapKit.git", branch: "develop"),
        .package(url: "https://github.com/Alamofire/Alamofire.git", .upToNextMajor(from: "5.6.1")),
        .package(url: "https://github.com/SwiftyJSON/SwiftyJSON.git", from: "4.0.0")
    ],
    targets: [
        .target(
            name: "LoginModule",
            dependencies: ["CommonModule", "CoordinatorAbstractModule", "UserRepositoryModule", "SnapKit", "Alamofire", "SwiftyJSON"]),
        .testTarget(
            name: "LoginModuleTests",
            dependencies: ["LoginModule"]),
    ]
)
