
import UIKit
import CommonModule
import SnapKit

public class LoginViewController: UIViewController {
    
    // MARK: - UI Components
    
    private lazy var loginLabel: UILabel = CustomLabel().configureLabel(text: "Log in", fontSize: 34)
    private lazy var usernameTextField: UITextField = CustomTextField().configureTextField(placeholder: "login")
    private lazy var passwordTextField: UITextField = CustomTextField().configureTextField(placeholder: "password")
    private lazy var button: UIButton = CustomButton().configureColoredButton(title: "Log in", color: .customGray, sideInset: 50)
    private lazy var stackView: UIStackView = UIStackView()
    
    // MARK: - View Model
    
    var viewModel: LoginViewModelProtocol!
    
    // MARK: - Init
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    // MARK: - Private
    
    private func configureView() {
        view.backgroundColor = .customWhite
        addViews()
        configureTextFields()
        configureButton()
        configureStackView()
        configureLayouts()
    }
    
    private func addViews() {
        view.addSubview(loginLabel)
        view.addSubview(stackView)
        view.addSubview(button)
    }
    
    private func configureTextFields() {
        usernameTextField.addTarget(self, action: #selector(textFieldsIsNotEmpty(sender:)), for: .editingChanged)
        passwordTextField.isSecureTextEntry = true
        passwordTextField.addTarget(self, action: #selector(textFieldsIsNotEmpty(sender:)), for: .editingChanged)
    }
    
    private func configureButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "arrow.uturn.backward")?.withTintColor(.customBlack, renderingMode: .alwaysOriginal), style: .plain, target: self, action: #selector(backButtonAction))
        button.isEnabled = false
        button.addTarget(self, action: #selector(loginButtonAction), for: .touchUpInside)
    }
    
    private func configureStackView() {
        stackView.addArrangedSubview(usernameTextField)
        stackView.addArrangedSubview(passwordTextField)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 11
    }
    
    private func configureLayouts() {
        loginLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(35)
            make.left.equalToSuperview().inset(16)
        }
        stackView.snp.makeConstraints { make in
            make.top.equalTo(loginLabel.snp.bottom).inset(-35)
            make.left.right.equalToSuperview().inset(16)
        }
        button.snp.makeConstraints { make in
            make.top.equalTo(stackView.snp.bottom).inset(-21)
            make.left.right.equalToSuperview().inset(16)
        }
    }
    
    // MARK: - objc
    
    @objc private func loginButtonAction() {
        viewModel.didTapButton(username: usernameTextField.text ?? "", password: passwordTextField.text ?? "")
    }
    
    @objc private func backButtonAction() {
        viewModel.didTapBackButton()
    }
    
    @objc private func textFieldsIsNotEmpty(sender : UITextField) {
        sender.text = sender.text?.trimmingCharacters(in: .whitespaces)
        
        guard let login = usernameTextField.text, !login.isEmpty,
              let password = passwordTextField.text, !password.isEmpty
        else {
            button.isEnabled = false
            button.configuration?.baseBackgroundColor = .customGray
            return
        }
        button.isEnabled = true
        button.configuration?.baseBackgroundColor = .customBlue
    }
}
