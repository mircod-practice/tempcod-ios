
import UIKit
import CoordinatorAbstractModule
import CommonModule

public class LoginCoordinator: CoordinatorAbstract {
    
    // MARK: - Properties
    
    public var navigationController: UINavigationController
    public var coordinators: [CoordinatorAbstract] = []
    
    public var flowCompletionHandler: (((NextRoute)?) -> Void)?
    
    // MARK: - Init
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    // MARK: - Public
    
    public func start() {
        let viewModel = LoginViewModel()
        let view = LoginViewController()
        view.viewModel = viewModel
        
        viewModel.completionHandler = { [weak self] route in
            switch route {
            case .main:
                self?.showMain()
            case .register:
                self?.showRegister()
            case .dataError:
                self?.showDataError()
            case .backButton:
                self?.showWelcomeModule()
            }
        }
        navigationController.show(view, sender: self)
    }
    
    // MARK: - Private
    
    private func showMain() {
        flowCompletionHandler?(.main)
    }
    
    private func showDataError() {
        let vc = CustomViewController()
        vc.dialogStateImageView.image = UIImage(named: "error")
        vc.largeLabel.text = "Failed to login"
        vc.smallLabel.text = "No active account found with the given credentials"
        vc.coloredButton.setTitle("Try again", for: .normal)
        vc.simpleButton.setTitle("Register", for: .normal)
        vc.modalPresentationStyle = .overCurrentContext
        navigationController.present(vc, animated: true)
        vc.coloredButton.addTarget(self, action: #selector(dismissViewController), for: .touchUpInside)
        vc.simpleButton.addTarget(self, action: #selector(showRegister), for: .touchUpInside)
    }
    
    private func showWelcomeModule() {
        flowCompletionHandler?(nil)
        navigationController.popViewController(animated: true)
    }
    
    // MARK: - objc
    
    @objc private func dismissViewController() {
        navigationController.dismiss(animated: true)
    }
    
    @objc private func showRegister() {
        flowCompletionHandler?(.register)
        navigationController.dismiss(animated: true)
    }
}
