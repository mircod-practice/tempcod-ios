
import Foundation
import Alamofire
import SwiftyJSON
import UserRepositoryModule
import CommonModule

public enum NextRoute {
    case main
    case register
    case dataError
    case backButton
}

protocol LoginViewModelProtocol {
    var completionHandler: ((NextRoute) -> Void)? { get set }
    func didTapButton(username: String, password: String)
    func didTapBackButton()
}

public class LoginViewModel: LoginViewModelProtocol {
    
    // MARK: - Properties
    
    var completionHandler: ((NextRoute) -> Void)?
    
    // MARK: - Public
    
    public func didTapButton(username: String, password: String) {
        checkData(username: username, password: password)
    }
    
    public func didTapBackButton() {
        completionHandler?(.backButton)
    }
    
    // MARK: - Private
    
    private func checkData(username: String, password: String) {
        guard let signInUrl = URL(string: CustomPath.absolutePath.rawValue + CustomPath.signInPath.rawValue) else { return }
        let params: [String: Any] = [
            "username": "\(username)",
            "password": "\(password)",
        ]
        AF.request(signInUrl, method: .post, parameters: params).response { [weak self] response in
            switch response.result {
            case .success(let value):
                let json = JSON(value!)
                if(response.response?.statusCode == 200) {
                    self?.completionHandler?(.main)
                    let accessToken = json["access"].stringValue
                    let refreshToken = json["refresh"].stringValue
                    UserRepistory.shared.saveTokens(accessToken: accessToken, refreshToken: refreshToken)
                } else {
                    self?.completionHandler?(.dataError)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
