
import UIKit
import CoordinatorAbstractModule
import WelcomeModule
import RegistrationModule
import LoginModule
import TabBarModule
import Alamofire
import UserRepositoryModule
import CommonModule

class AppCoordinator: CoordinatorAbstract {
    
    var window: UIWindow?
    var welcomeViewController: WelcomeViewControllerProtocol?
    var navigationController: UINavigationController = {
        let navigationController = UINavigationController()
        return navigationController
    }()
    
    var coordinators: [CoordinatorAbstract] = []
    
    init(window: UIWindow?) {
        self.window = window
    }
    
    func start() {
        window?.rootViewController = navigationController
        checkAccessToken()
    }
    
    private func checkAccessToken() {
        guard let checkTokenUrl = URL(string: CustomPath.absolutePath.rawValue + CustomPath.verifyTokenPath.rawValue) else { return }
        let accessToken = UserRepistory.shared.getAccessToken()
        let params: [String: Any] = [
            "token": accessToken ?? ""
        ]
        AF.request(checkTokenUrl, method: .post, parameters: params).response { [weak self] response in
            switch response.result {
            case .success(_):
                if response.response?.statusCode == 200 {
                    self?.showMain()
                } else {
                    self?.showWelcome()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func showWelcome() {
        welcomeViewController = WelcomeViewController(completionHandler: { [weak self] route in
            switch route {
            case .login: self?.showLogin()
            case .register: self?.showRegistration()
            }
        })
        navigationController.show(welcomeViewController as! UIViewController, sender: self)
    }
    
    private func showMain() {
        let tabBar = TabCoordinator(navigationController: navigationController)
        coordinators.append(tabBar)
        tabBar.tabBarFlowCompletionHandler = { [weak self] in
            tabBar.coordinators.removeAll()
            self?.removeChildCoordinator(tabBar)
            self?.showWelcome()
        }
        tabBar.start()
    }
    
    private func showLogin() {
        let loginCoordinator = LoginCoordinator(navigationController: navigationController)
        coordinators.append(loginCoordinator)
        
        loginCoordinator.flowCompletionHandler = { [weak self] route in
            self?.removeChildCoordinator(loginCoordinator)
            guard route != nil else { return }
            if(route == .main) {
                self?.showMain()
            } else if (route == .register) {
                self?.navigationController.popToRootViewController(animated: false)
                self?.showRegistration()
            }
        }
        loginCoordinator.start()
    }
    
    private func showRegistration() {
        let registrationCoordinator = RegistrationCoordinator(navigationController: navigationController)
        coordinators.append(registrationCoordinator)
        
        registrationCoordinator.flowCompletionHandler = { [weak self] route in
            self?.removeChildCoordinator(registrationCoordinator)
            guard route != nil else { return }
            if(route == .login) {
                self?.navigationController.popToRootViewController(animated: true)
                self?.showLogin()
            }
        }
        registrationCoordinator.start()
    }
}
