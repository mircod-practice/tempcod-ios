// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SettingsModule",
    platforms: [.iOS(.v15)],
    products: [
        .library(
            name: "SettingsModule",
            targets: ["SettingsModule"]),
    ],
    dependencies: [
        .package(path: "../CommonModule"),
        .package(path: "../CoordinatorAbstractModule"),
        .package(url: "https://github.com/SnapKit/SnapKit.git", branch: "develop"),
    ],
    targets: [
        .target(
            name: "SettingsModule",
            dependencies: ["CommonModule", "CoordinatorAbstractModule" , "SnapKit"]),
        .testTarget(
            name: "SettingsModuleTests",
            dependencies: ["SettingsModule"]),
    ]
)
