
import Foundation

public enum NextRoute {
    case temperature
    case about
    case settings
}

protocol SettingsViewModelProtocol {
    var completionHandler: ((NextRoute) -> Void)? { get set }
    func didTapCell(row: Int)
    func didTapSaveButton()
    func didTapBackButton()
}

public class SettingsViewModel: SettingsViewModelProtocol {
    
    // MARK: - Properties
    
    var completionHandler: ((NextRoute) -> Void)?
    
    // MARK: - Public
    
    public func didTapCell(row: Int) {
        if row == 0 {
            completionHandler?(.temperature)
        }
        else if row == 2 {
            completionHandler?(.about)
        }
    }
    
    public func didTapSaveButton() {
        //...
        completionHandler?(.settings)
    }
    
    public func didTapBackButton() {
        completionHandler?(.settings)
    }
}
