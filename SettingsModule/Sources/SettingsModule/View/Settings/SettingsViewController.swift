
import UIKit
import CommonModule
import SnapKit

public class SettingsViewController: UIViewController {
    // MARK: - UI Components

    private lazy var backgroundGradientView: UIView = CustomBackground().configureCustomBackground(isGearshapeNeeded: true, view: self.view)
    private lazy var titleLabel: UILabel = CustomLabel().configureLabel(text: "Settings", fontSize: 26)
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "\(UITableViewCell.self)")
        return tableView
    }()
    
    private lazy var notificationSwitch: UISwitch = CustomSwitch().configureSwitch()
    private lazy var appVersionLabel: UILabel = UILabel()
    
    // MARK: - Data
    
    private var settingsSymbols: [UIImage?] = [UIImage(systemName: "thermometer"), UIImage(systemName: "bell"), UIImage(systemName: "lightbulb")]
    private var settingsLabels: [String] = ["Temperature settings", "Push-notifications", "About application"]
    
    // MARK: - View Model
    
    var viewModel: SettingsViewModelProtocol!
    
    // MARK: - Init
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    // MARK: - Private
    
    private func configureView() {
        addViews()
        configureAppVersionlabel()
        configureTableView()
        configureLayouts()
    }
    
    private func addViews() {
        view.addSubview(backgroundGradientView)
        view.addSubview(titleLabel)
        view.addSubview(tableView)
        view.addSubview(notificationSwitch)
        view.addSubview(appVersionLabel)
    }
    
    private func configureAppVersionlabel() {
        appVersionLabel.text = "Tempcod application version 1.043.0433"
        appVersionLabel.font = .label.withSize(12)
        appVersionLabel.textColor = .customGray
    }
    
    private func configureTableView() {
        tableView.backgroundColor = .clear
        tableView.separatorInset = UIEdgeInsets.init(top: 0, left: 13, bottom: 0, right: 13)
        tableView.rowHeight = 60
    }
    
    private func configureLayouts() {
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalToSuperview().inset(30)
        }
        tableView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(10)
            make.left.right.equalToSuperview().inset(18)
            make.height.equalTo(tableView.rowHeight * 3)
        }
        notificationSwitch.snp.makeConstraints { make in
            make.top.equalTo(tableView.snp.top).inset(74)
            make.right.equalTo(tableView.snp.right).inset(15)
        }
        appVersionLabel.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).inset(30)
            make.left.equalToSuperview().inset(85)
        }
    }
    
    private func configureCell(cell: UITableViewCell, row: Int) -> UITableViewCell {
        var content = cell.defaultContentConfiguration()
        content.image = settingsSymbols[row]
        content.text = settingsLabels[row]
        content.imageProperties.tintColor = .customGray
        content.imageProperties.maximumSize = CGSize(width: 20, height: 25)
        content.textProperties.font = .label
        cell.backgroundColor = .clear
        cell.contentConfiguration = content
        return cell
    }
}

// MARK: - Table View Data Source

extension SettingsViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        3
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        return configureCell(cell: cell, row: indexPath.row)
    }
}

//MARK: - Table View Delegate

extension SettingsViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if (indexPath.row == 0 || indexPath.row == 2) {
            return indexPath
        }
        return nil
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.didTapCell(row: indexPath.row)
    }
}

