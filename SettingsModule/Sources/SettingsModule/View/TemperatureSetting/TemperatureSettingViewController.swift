
import CommonModule
import SnapKit
import UIKit

public class TemperatureSettingViewController: UIViewController {
    
    // MARK: - UI Components

    private lazy var gradientBackgroundView: UIView = CustomBackground(frame: .zero).configureCustomBackground(isGearshapeNeeded: true, view: self.view)
    private lazy var titleButton: UIButton = CustomButton().configureBackButton(title: "Temperature settings")
    private lazy var descriptionLabel: UILabel = CustomLabel().configureLabel(text: "Temperature unit", fontSize: 16)
    private lazy var tempSegmentedControl: UISegmentedControl = CustomSegmentedControl(items: ["Celsius", "Fahrenheit"]).configureSegmentedControl()
    private lazy var saveButton: UIButton = CustomButton().configureColoredButton(title: "Save", color: .customBlue, sideInset: 130)
    
    // MARK: - View Model
    
    var viewModel: SettingsViewModelProtocol!
    
    // MARK: - Init
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    // MARK: - Private
    
    private func configureView() {
        addViews()
        configureSegmentedControl()
        configureBackButton()
        configureSaveButton()
        configureNavItem()        
        configureLayouts()
    }
    
    private func addViews() {
        view.addSubview(gradientBackgroundView)
        view.addSubview(titleButton)
        view.addSubview(descriptionLabel)
        view.addSubview(tempSegmentedControl)
        view.addSubview(saveButton)
    }
    
    private func configureSegmentedControl() {
        if TemperatureUnitService.shared.isCelsius {
            tempSegmentedControl.selectedSegmentIndex = 0
        } else {
            tempSegmentedControl.selectedSegmentIndex = 1
        }
    }
    
    private func configureBackButton() {
        titleButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
    }
    
    private func configureSaveButton() {
        saveButton.addTarget(self, action: #selector(saveButtonTapped), for: .touchUpInside)
    }
    
    private func configureNavItem() {
        navigationItem.hidesBackButton = true
    }
    
    private func configureLayouts() {
        titleButton.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalToSuperview().inset(20)
        }
        
        descriptionLabel.snp.makeConstraints { make in
            make.top.equalTo(titleButton.snp.bottom).offset(20)
            make.left.equalToSuperview().inset(30)
        }
        
        tempSegmentedControl.snp.makeConstraints{ make in
            make.top.equalTo(descriptionLabel.snp.bottom).offset(20)
            make.left.right.equalToSuperview().inset(30)
            make.height.equalTo(40)
        }
        
        saveButton.snp.makeConstraints { make in
            make.top.equalTo(tempSegmentedControl.snp.bottom).offset(60)
            make.centerX.equalToSuperview()
        }
    }
    
    // MARK: - objc
    
    @objc func backButtonTapped() {
        viewModel.didTapBackButton()
    }
    
    @objc func saveButtonTapped() {
        if tempSegmentedControl.selectedSegmentIndex == 0 {
            TemperatureUnitService.shared.isCelsiusClosure?(true)
            TemperatureUnitService.shared.isCelsius = true
        } else {
            TemperatureUnitService.shared.isCelsiusClosure?(false)
            TemperatureUnitService.shared.isCelsius = false
        }
        viewModel.didTapSaveButton()
    }
}
