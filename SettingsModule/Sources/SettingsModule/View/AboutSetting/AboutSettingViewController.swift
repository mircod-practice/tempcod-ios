
import UIKit
import SnapKit
import CommonModule

public class AboutSettingViewController: UIViewController {
    
    // MARK: - UI Components

    private lazy var gradientBackgroundView: UIView = CustomBackground(frame: .zero).configureCustomBackground(isGearshapeNeeded: true, view: self.view)
    private lazy var titleButton: UIButton = CustomButton().configureBackButton(title: "About application")
    private lazy var firstTextView: UITextView = UITextView()
    private lazy var secondTextView: UITextView = UITextView()
    
    // MARK: - View
    
    var viewModel: SettingsViewModelProtocol!
    
    // MARK: - Init
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    // MARK: - Private
    
    private func configureView() {
        addViews()
        configureNavItem()
        configureBackButton()
        configureTextViews()
        configureLayouts()
    }
    
    private func addViews() {
        view.addSubview(gradientBackgroundView)
        view.addSubview(titleButton)
        view.addSubview(firstTextView)
        view.addSubview(secondTextView)
    }
    
    private func configureNavItem() {
        navigationItem.hidesBackButton = true
    }
    
    private func configureBackButton() {
        titleButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
    }
    
    private func configureTextViews() {
        firstTextView.font = .label
        firstTextView.backgroundColor = .clear
        secondTextView.font = .label
        secondTextView.backgroundColor = .clear
        firstTextView.text.append(getStringFromFile(part: 1))
        secondTextView.text.append(getStringFromFile(part: 2))
        secondTextView.text.append(getStringFromFile(part: 3))
    }
    
    private func configureLayouts() {
        titleButton.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalToSuperview().inset(20)
        }
        
        firstTextView.snp.makeConstraints { make in
            make.top.equalTo(titleButton.snp.bottom).offset(15)
            make.left.right.equalToSuperview().inset(30)
            make.height.equalTo(130)
        }
        
        secondTextView.snp.makeConstraints { make in
            make.top.equalTo(firstTextView.snp.bottom).offset(10)
            make.left.right.bottom.equalToSuperview().inset(30)
        }
    }
    
    private func getStringFromFile(part: Int) -> String {
        var rows = [String]()
        if let path = Bundle.main.path(forResource: "aboutApp", ofType: "txt") {
            if let text = try? String(contentsOfFile: path) {
                rows = text.components(separatedBy: "\n")
            }
        }
        var text: String = ""
        for i in 0...part - 1 {
            text = rows[i]
        }
        return text
    }
    
    // MARK: - objc
    
    @objc func backButtonTapped() {
        viewModel.didTapBackButton()
    }
}
