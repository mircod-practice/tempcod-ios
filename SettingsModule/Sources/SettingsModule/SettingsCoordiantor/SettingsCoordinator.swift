
import Foundation
import CoordinatorAbstractModule
import UIKit

public class SettingsCoordinator: CoordinatorAbstract {
    // MARK: - Properties
    public var navigationController: UINavigationController
    public var coordinators: [CoordinatorAbstract] = []
        
    let viewModel = SettingsViewModel()
    
    // MARK: - Init
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    // MARK: - Public
    
    public func start() {
        let view = SettingsViewController()
        view.viewModel = viewModel
        
        viewModel.completionHandler = { [ weak self ] route in
            switch route {
            case .temperature:
                self?.showTemperatureSetting()
            case .about:
                self?.showAboutSetting()
            case .settings:
                self?.showSettingsViewController()
            }
        }
        navigationController.show(view, sender: self)
    }
    
    // MARK: - Private
    
    private func showTemperatureSetting() {
        let temperatureSettingViewController = TemperatureSettingViewController()
        temperatureSettingViewController.viewModel = viewModel
        navigationController.show(temperatureSettingViewController, sender: self)
    }
    
    private func showAboutSetting() {
        let aboutSettingViewController = AboutSettingViewController()
        aboutSettingViewController.viewModel = viewModel
        navigationController.show(aboutSettingViewController, sender: self)
    }
    
    private func showSettingsViewController() {
        navigationController.popViewController(animated: true)
    }
}
