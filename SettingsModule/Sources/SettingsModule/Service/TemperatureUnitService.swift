
import Foundation

public class TemperatureUnitService {
    public static var shared: TemperatureUnitService = TemperatureUnitService()
    
    public var isCelsius: Bool = true
    public var isCelsiusClosure: ((Bool) -> Void)?
    
    public func getFahrenheit(celsius: Double) -> Double {
        return 9/5*celsius + 32
    }
    
    public func getCelsius(fahrenheit: Double) -> Double {
        return 5/9*(fahrenheit-32)
    }
}
