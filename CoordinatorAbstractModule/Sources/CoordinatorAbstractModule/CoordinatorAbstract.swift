
import UIKit

public protocol CoordinatorAbstract: AnyObject {
    
    var navigationController: UINavigationController { get }
    var coordinators: [CoordinatorAbstract] { get set }
    
    func start()
}

extension CoordinatorAbstract {
    
    public func removeChildCoordinator(_ coordinator: CoordinatorAbstract) {
        
        guard let index = coordinators.firstIndex(where: { $0 === coordinator }) else { return }
        coordinators.remove(at: index)
    }
}
