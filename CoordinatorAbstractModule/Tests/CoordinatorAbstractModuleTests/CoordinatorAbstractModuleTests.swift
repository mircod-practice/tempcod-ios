import XCTest
@testable import CoordinatorAbstractModule

final class CoordinatorAbstractModuleTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(CoordinatorAbstractModule().text, "Hello, World!")
    }
}
