// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "UserRepositoryModule",
    platforms: [.iOS(.v15)],
    products: [
        .library(
            name: "UserRepositoryModule",
            targets: ["UserRepositoryModule"]),
    ],
    dependencies: [
        .package(url: "https://github.com/kishikawakatsumi/KeychainAccess.git", from: "3.0.0")
    ],
    targets: [
        .target(
            name: "UserRepositoryModule",
            dependencies: ["KeychainAccess"]),
        .testTarget(
            name: "UserRepositoryModuleTests",
            dependencies: ["UserRepositoryModule"]),
    ]
)
