
import Foundation

public struct User {
    var username: String?
    var firstname: String?
    var lastname: String?
    public init(username: String?, firstname: String?, lastname: String?) {
        self.username = username
        self.firstname = firstname
        self.lastname = lastname
    }
}
