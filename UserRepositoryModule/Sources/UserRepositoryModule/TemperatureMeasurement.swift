
import Foundation

public struct TemperatureMeasurement {
    public var temperature: Double?
    public var date: String?
    
    public init(temperature: Double?, date: String?) {
        self.temperature = temperature
        self.date = date
    }
}
