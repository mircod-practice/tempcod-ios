
import Foundation
import KeychainAccess

public protocol UserRepositoryProtocol {
    func saveUser(user: User)
    func getUser() -> User
    func saveTokens(accessToken: String, refreshToken: String)
    func getAccessToken() -> String?
    func getRefreshToken() -> String?
}

public class UserRepistory: UserRepositoryProtocol {

    public static let shared = UserRepistory()
    
    let keychain = Keychain()
    
    public func saveUser(user: User) {
        keychain["username"] = user.username
        keychain["firstname"] = user.firstname
        keychain["lastname"] = user.lastname
    }
    
    public func getUser() -> User {
        guard let username = keychain["username"], let firstname = keychain["firstname"], let lastname = keychain["lastname"] else { return User(username: "", firstname: "", lastname: "")}
        return User(username: username, firstname: firstname, lastname: lastname)
    }
    
    public func saveTokens(accessToken: String, refreshToken: String) {
        keychain["access"] = accessToken
        keychain["refresh"] = refreshToken
    }
    
    public func getName() -> String {
        guard let firstname = keychain["firstname"], let lastname = keychain["lastname"] else { return ""}
        return "\(firstname) \(lastname)"
    }
    
    public func getUsername() -> String {
        guard let username = keychain["username"] else { return ""}
        return username
    }
    
    public func getAccessToken() -> String? {
        return keychain["access"]
    }
    
    public func getRefreshToken() -> String? {
        return keychain["refresh"]
    }
    
    public func removeTokens() {
        keychain["access"] = nil
        keychain["refresh"] = nil
    }
}
