import XCTest
@testable import UserRepositoryModule

final class UserRepositoryModuleTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(UserRepositoryModule().text, "Hello, World!")
    }
}
