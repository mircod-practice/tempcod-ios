import XCTest
@testable import ProfileModule

final class ProfileModuleTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(ProfileModule().text, "Hello, World!")
    }
}
