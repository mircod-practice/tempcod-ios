
import UIKit
import CoordinatorAbstractModule
import Photos
import PhotosUI

public class ProfileCoordinator: CoordinatorAbstract {
    
    // MARK: - Properties
    
    public var navigationController: UINavigationController
    public var coordinators: [CoordinatorAbstract] = []
    
    public var flowCompletionHandler: ((NextRoute) -> Void)?
    
    // MARK: - Init
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    // MARK: - Public
    
    public func start() {
        let viewModel = ProfileViewModel()
        let view = ProfileViewController()
        view.viewModel = viewModel
        viewModel.completionHandler = { [weak self] route in
            switch route {
            case .logout:
                self?.flowCompletionHandler?(route)
            case .camera:
                self?.showCamera(vc: view)
            case .gallery:
                self?.showGallery(vc: view)
            }
        }
        navigationController.show(view, sender: self)
    }
    
    // MARK: - Private
    
    private func showCamera(vc: ProfileViewController) {
        let pickerController = UIImagePickerController()
        pickerController.sourceType = .camera
        pickerController.allowsEditing = true
        pickerController.delegate = vc
        navigationController.present(pickerController, animated: true)
    }
    
    private func showGallery(vc: ProfileViewController) {
        var pickerConfig = PHPickerConfiguration(photoLibrary: .shared())
        pickerConfig.filter = .images
        pickerConfig.selectionLimit = 1
        let pickerViewController = PHPickerViewController(configuration: pickerConfig)
        pickerViewController.delegate = vc
        navigationController.present(pickerViewController, animated: true)
    }
}
