
import Foundation
import Alamofire
import UserRepositoryModule
import CommonModule

public enum NextRoute {
    case logout
    case camera
    case gallery
}

public protocol ProfileViewModelProtocol {
    func didTapLogoutButton()
    func didTapCameraButton()
    func didTapGalleryButton()
    var completionHandler: ((NextRoute) -> Void)? { get set }
}

public class ProfileViewModel: ProfileViewModelProtocol {
    
    // MARK: - Properties
    
    public var completionHandler: ((NextRoute) -> Void)?
    
    // MARK: - Public
    
    public func didTapLogoutButton() {
        sendLogout()
        completionHandler?(.logout)
    }
    
    public func didTapCameraButton() {
        completionHandler?(.camera)
    }
    
    public func didTapGalleryButton() {
        completionHandler?(.gallery)
    }
    
    // MARK: - Private
    
    private func sendLogout() {
        guard let url = URL(string: CustomPath.absolutePath.rawValue +  CustomPath.logoutPath.rawValue) else { return }
        let refreshToken = UserRepistory.shared.getRefreshToken()
        let params: [String: Any] = [
            "refresh": refreshToken ?? "",
        ]
        guard let accessToken = UserRepistory.shared.getAccessToken() else { return }
        AF.request(url, method: .post, parameters: params, headers: HTTPHeaders([HTTPHeader(name: "Authorization", value: "Bearer \(accessToken)")])).response { response in
            switch response.result {
            case .success(_):
                print(response.response?.statusCode ?? 0)
                if response.response?.statusCode == 204 {
                    UserRepistory.shared.removeTokens()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
