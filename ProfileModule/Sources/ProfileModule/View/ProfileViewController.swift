
import UIKit
import SnapKit
import CommonModule
import UserRepositoryModule
import PhotosUI

class ProfileViewController: UIViewController {
    
    // MARK: - UI Components
    
    private lazy var titleLabel: UILabel = CustomLabel().configureLabel(text: "Profile", fontSize: 26)
    private lazy var avatarImageView: UIImageView = UIImageView(image: UIImage(named: "no image"))
    private lazy var nameLabel: UILabel = CustomLabel().configureLabel(text: UserRepistory.shared.getName(), fontSize: 26)
    private lazy var usernameLabel: UILabel = CustomLabel().configureBlueLabel(username: UserRepistory.shared.getUsername())
    private lazy var lineView: UIView = UIView()
    private lazy var logoutButton: UIButton = UIButton()
    private lazy var cameraButton: UIButton = UIButton()
    private lazy var galleryButton: UIButton = UIButton()
    private lazy var whiteView: UIView = UIView()
    
    // MARK: - View Model
    
    var viewModel: ProfileViewModelProtocol!
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    // MARK: - Private
    
    private func configureView() {
        view = CustomBackground().configureCustomBackground(isGearshapeNeeded: false, view: self.view)
        addViews()
        configureWhiteView()
        configureLineView()
        configureLogoutButton()
        configureLayouts()
    }
    
    private func addViews() {
        view.addSubview(titleLabel)
        view.addSubview(whiteView)
        view.addSubview(nameLabel)
        view.addSubview(usernameLabel)
        view.addSubview(lineView)
        view.addSubview(logoutButton)
    }
    
    private func configureWhiteView() {
        whiteView.backgroundColor = .customWhite
        whiteView.layer.cornerRadius = 70
        avatarImageView.layer.cornerRadius = 60
        avatarImageView.clipsToBounds = true
        whiteView.addSubview(cameraButton)
        whiteView.addSubview(galleryButton)
        whiteView.addSubview(avatarImageView)
        configurePhotoButtons()
    }
    
    private func configurePhotoButtons() {
        cameraButton.setImage(UIImage(named: "сamera btn"), for: .normal)
        cameraButton.addTarget(self, action: #selector(cameraAction), for: .touchUpInside)
        galleryButton.setImage(UIImage(named: "gallery btn"), for: .normal)
        galleryButton.addTarget(self, action: #selector(galleryAction), for: .touchUpInside)
    }
    
    private func configureLineView() {
        lineView.backgroundColor = .customGray
    }
    
    private func configureLogoutButton() {
        logoutButton.addTarget(self, action: #selector(logoutAction), for: .touchUpInside)
        var configuration = UIButton.Configuration.filled()
        configuration.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: 7, bottom: 20, trailing: 50)
        configuration.baseForegroundColor = .customBlack
        configuration.baseBackgroundColor = .clear
        configuration.attributedTitle = AttributedString("Logout", attributes: AttributeContainer([NSAttributedString.Key.font: UIFont.label]))
        configuration.imagePadding = 35
        
        let chevronConfiguration = UIImage.SymbolConfiguration(pointSize: 9, weight: .heavy)
        logoutButton.setImage(UIImage(systemName: "chevron.right", withConfiguration: chevronConfiguration), for: .normal)
        logoutButton.semanticContentAttribute = .forceRightToLeft
        logoutButton.configuration = configuration
        
    }
    
    private func configureLayouts() {
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalToSuperview().inset(30)
        }
        whiteView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).inset(-100)
            make.right.left.equalToSuperview().inset(26)
            make.centerX.equalTo(view.snp.centerX)
            make.height.equalTo(135)
        }
        avatarImageView.snp.makeConstraints { make in
            make.height.width.equalTo(120)
            make.centerX.equalTo(view.snp.centerX)
            make.top.equalTo(whiteView.snp.top).inset(7)
        }
        cameraButton.snp.makeConstraints { make in
            make.top.equalTo(whiteView.snp.top).inset(53)
            make.right.equalTo(avatarImageView.snp.left).inset(-27)
            make.height.equalTo(53)
            make.width.equalTo(39)
        }
        galleryButton.snp.makeConstraints { make in
            make.top.equalTo(whiteView.snp.top).inset(53)
            make.left.equalTo(avatarImageView.snp.right).inset(-27)
            make.height.equalTo(53)
            make.width.equalTo(39)
        }
        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(avatarImageView.snp.bottom).inset(-11)
            make.centerX.equalTo(view.snp.centerX)
            make.right.left.equalToSuperview().inset(16)
        }
        usernameLabel.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom).inset(-17)
            make.centerX.equalTo(view.snp.centerX)
        }
        lineView.snp.makeConstraints { make in
            make.top.equalTo(usernameLabel.snp.bottom).inset(-58)
            make.height.equalTo(1)
            make.right.left.equalToSuperview().inset(30)
        }
        logoutButton.snp.makeConstraints { make in
            make.top.equalTo(lineView.snp.bottom).inset(-40)
            make.centerX.equalTo(view.snp.centerX)
        }
    }
    
    // MARK: - objc
    
    @objc private func logoutAction() {
        viewModel.didTapLogoutButton()
    }
    
    @objc private func cameraAction() {
        viewModel.didTapCameraButton()
    }
    
    @objc private func galleryAction() {
        viewModel.didTapGalleryButton()
    }
}

// MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.editedImage] as? UIImage else { return }
        avatarImageView.image = image
    }
}

// MARK: - PHPickerViewControllerDelegate

extension ProfileViewController: PHPickerViewControllerDelegate {
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        picker.dismiss(animated: true, completion: nil)
        results.forEach { [weak self] result in
            result.itemProvider.loadObject(ofClass: UIImage.self) { reading, error in
                guard let image = reading as? UIImage, error == nil else { return }
                DispatchQueue.main.async {
                    self?.avatarImageView.image = image
                }
            }
        }
    }
}
