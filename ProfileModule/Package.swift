// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ProfileModule",
    platforms: [.iOS(.v15)],
    products: [
        .library(
            name: "ProfileModule",
            targets: ["ProfileModule"]),
    ],
    dependencies: [
        .package(path: "../CommonModule"),
        .package(path: "../CoordinatorAbstractModule"),
        .package(url: "https://github.com/SnapKit/SnapKit.git", branch: "develop"),
        .package(url: "https://github.com/Alamofire/Alamofire.git", .upToNextMajor(from: "5.6.1")),
        .package(path: "../UserRepositoryModule")
    ],
    targets: [
        .target(
            name: "ProfileModule",
            dependencies: ["CommonModule", "CoordinatorAbstractModule" , "SnapKit", "Alamofire", "UserRepositoryModule"]),
        .testTarget(
            name: "ProfileModuleTests",
            dependencies: ["ProfileModule"]),
    ]
)
