import XCTest
@testable import RegistrationModule

final class RegistrationModuleTests: XCTestCase {
    func testExample() throws {
        XCTAssertEqual(RegistrationModule().text, "Hello, World!")
    }
}
