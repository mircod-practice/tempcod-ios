// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "RegistrationModule",
    platforms: [.iOS(.v15)],
    products: [
        .library(
            name: "RegistrationModule",
            targets: ["RegistrationModule"]),
    ],
    dependencies: [
        .package(path: "../CommonModule"),
        .package(path: "../CoordinatorAbstractModule"),
        .package(url: "https://github.com/SnapKit/SnapKit.git", branch: "develop"),
        .package(url: "https://github.com/Alamofire/Alamofire.git", .upToNextMajor(from: "5.6.1")),
        .package(url: "https://github.com/SwiftyJSON/SwiftyJSON.git", from: "4.0.0")
    ],
    targets: [
        .target(
            name: "RegistrationModule",
            dependencies: ["CommonModule", "CoordinatorAbstractModule" , "SnapKit", "Alamofire", "SwiftyJSON"]),
        .testTarget(
            name: "RegistrationModuleTests",
            dependencies: ["RegistrationModule"]),
    ]
)
