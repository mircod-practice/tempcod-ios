
import UIKit
import CommonModule
import SnapKit

public class RegistrationViewController: UIViewController {
    
    // MARK: - UI Components
    
    private lazy var registrationLabel: UILabel = CustomLabel().configureLabel(text: "Register", fontSize: 34)
    private lazy var usernameTextField: UITextField = CustomTextField().configureTextField(placeholder: "login")
    private lazy var passwordTextField: UITextField = CustomTextField().configureTextField(placeholder: "password")
    private lazy var firstNameTextField: UITextField = CustomTextField().configureTextField(placeholder: "first name")
    private lazy var lastNameTextField: UITextField = CustomTextField().configureTextField(placeholder: "last name")
    private lazy var button: UIButton = CustomButton().configureColoredButton(title: "next", color: .customGray, sideInset: 50)
    private lazy var stackView: UIStackView = UIStackView()
    
    // MARK: - View Model
    
    var viewModel: RegistrationViewModelProtocol!
    
    // MARK: - Init
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    // MARK: - Private
    
    private func configureView() {
        view.backgroundColor = .customWhite
        addViews()
        configureTextFields()
        configureButton()
        configureStackView()
        configureLayouts()
    }
    
    private func addViews() {
        view.addSubview(registrationLabel)
        view.addSubview(stackView)
        view.addSubview(button)
    }
    
    private func configureTextFields() {
        usernameTextField.addTarget(self, action: #selector(textFieldsIsNotEmpty(sender:)), for: .editingChanged)
        passwordTextField.isSecureTextEntry = true
        passwordTextField.addTarget(self, action: #selector(textFieldsIsNotEmpty(sender:)), for: .editingChanged)
        firstNameTextField.addTarget(self, action: #selector(textFieldsIsNotEmpty(sender:)), for: .editingChanged)
        lastNameTextField.addTarget(self, action: #selector(textFieldsIsNotEmpty(sender:)), for: .editingChanged)
    }
    
    private func configureButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "arrow.uturn.backward")?.withTintColor(.black, renderingMode: .alwaysOriginal), style: .plain, target: self, action: #selector(backButtonAction))
        button.isEnabled = false
        button.addTarget(self, action: #selector(regButtonAction), for: .touchUpInside)
    }
    
    private func configureStackView() {
        stackView.addArrangedSubview(usernameTextField)
        stackView.addArrangedSubview(passwordTextField)
        stackView.addArrangedSubview(firstNameTextField)
        stackView.addArrangedSubview(lastNameTextField)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 11
    }
    
    private func configureLayouts() {
        registrationLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(35)
            make.left.equalToSuperview().inset(16)
        }
        stackView.snp.makeConstraints { make in
            make.top.equalTo(registrationLabel.snp.bottom).inset(-35)
            make.left.right.equalToSuperview().inset(16)
        }
        button.snp.makeConstraints { make in
            make.top.equalTo(stackView.snp.bottom).inset(-21)
            make.left.right.equalToSuperview().inset(16)
        }
    }
    
    // MARK: - objc
    
    @objc private func regButtonAction() {
        viewModel.didTapButton(username: usernameTextField.text ?? "", password: passwordTextField.text ?? "", firstName: firstNameTextField.text ?? "", lastName: lastNameTextField.text ?? "")
    }
    
    @objc private func backButtonAction() {
        viewModel.didTapBackButton()
    }
    
    @objc private func textFieldsIsNotEmpty(sender : UITextField) {
        sender.text = sender.text?.trimmingCharacters(in: .whitespaces)
        
        guard let login = usernameTextField.text, !login.isEmpty,
              let password = passwordTextField.text, !password.isEmpty,
              let firstName = firstNameTextField.text, !firstName.isEmpty,
              let lastName = lastNameTextField.text, !lastName.isEmpty
        else {
            button.isEnabled = false
            button.configuration?.baseBackgroundColor = .customGray
            return
        }
        button.isEnabled = true
        button.configuration?.baseBackgroundColor = .customBlue
    }
}
