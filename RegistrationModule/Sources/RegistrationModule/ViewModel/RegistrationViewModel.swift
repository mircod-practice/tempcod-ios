
import Foundation
import Alamofire
import SwiftyJSON
import CommonModule

public enum NextRoute {
    case login
    case usernameError
    case dataError
    case backButton
}

protocol RegistrationViewModelProtocol {
    var completionHandler: ((NextRoute) -> Void)? { get set }
    func didTapButton(username: String, password: String, firstName: String, lastName: String)
    func didTapBackButton()
}

public class RegistrationViewModel: RegistrationViewModelProtocol {
    
    // MARK: - Properties
    
    var completionHandler: ((NextRoute) -> Void)?
    
    // MARK: - Public
    
    public func didTapButton(username: String, password: String, firstName: String, lastName: String) {
        checkData(username: username, password: password, firstName: firstName, lastName: lastName)
    }
    
    public func didTapBackButton() {
        completionHandler?(.backButton)
    }
    
    // MARK: - Private
    
    private func checkData(username: String, password: String, firstName: String, lastName: String) {
        guard let url = URL(string: CustomPath.absolutePath.rawValue + CustomPath.signUpPath.rawValue) else { return }
        
        let params: [String: Any] = [
            "username": username,
            "first_name": firstName,
            "last_name": lastName,
            "password": password
        ]
        AF.request(url, method: .post, parameters: params).response { [weak self] response in
            switch response.result {
            case .success(let value):
                let json = JSON(value!)
                if(response.response?.statusCode == 201) {
                    self?.completionHandler?(.login)
                } else if json["username"][0].stringValue != "" {
                    self?.completionHandler?(.usernameError)
                } else {
                    self?.completionHandler?(.dataError)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
