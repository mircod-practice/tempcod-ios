
import Foundation
import CoordinatorAbstractModule
import UIKit
import CommonModule

public class RegistrationCoordinator: CoordinatorAbstract {
    
    // MARK: - Properties
    
    public var navigationController: UINavigationController
    public var coordinators: [CoordinatorAbstract] = []
    
    public var flowCompletionHandler: ((NextRoute?) -> Void)?
    
    // MARK: - Init
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    // MARK: - Public
    
    public func start() {
        let viewModel = RegistrationViewModel()
        let view = RegistrationViewController()
        view.viewModel = viewModel
        
        viewModel.completionHandler = { [weak self] route in
            switch route {
            case .login:
                self?.showLogin()
            case .usernameError:
                self?.showUsernameError()
            case .dataError:
                self?.showDataError()
            case .backButton:
                self?.showWelcomeModule()
            }
        }
        navigationController.show(view, sender: self)
    }
    
    // MARK: - Private
    
    private func showLogin() {
        flowCompletionHandler?(.login)
    }
    
    private func showUsernameError() {
        let vc = CustomViewController()
        vc.dialogStateImageView.image = UIImage(named: "error")
        vc.largeLabel.text = "Failed to register"
        vc.smallLabel.text = "User with this username already exists"
        vc.coloredButton.setTitle("Try again", for: .normal)
        vc.simpleButton.isHidden = false
        vc.simpleButton.layer.isHidden = true
        vc.modalPresentationStyle = .overCurrentContext
        navigationController.present(vc, animated: true)
        vc.coloredButton.addTarget(self, action: #selector(dismissViewController), for: .touchUpInside)
    }
    
    private func showDataError() {
        let vc = CustomViewController()
        vc.dialogStateImageView.image = UIImage(named: "error")
        vc.largeLabel.text = "Failed to register"
        vc.smallLabel.text = "Ensure password field has at least 8 characters\nFirstname and lastname must not contain numbers"
        vc.smallLabel.numberOfLines = 5
        vc.coloredButton.setTitle("Try again", for: .normal)
        vc.simpleButton.isHidden = false
        vc.simpleButton.layer.isHidden = true
        vc.modalPresentationStyle = .overCurrentContext
        navigationController.present(vc, animated: true)
        vc.coloredButton.addTarget(self, action: #selector(dismissViewController), for: .touchUpInside)
    }
    
    private func showWelcomeModule() {
        flowCompletionHandler?(nil)
        navigationController.popViewController(animated: true)
    }
    
    // MARK: - objc
    
    @objc private func dismissViewController() {
        navigationController.dismiss(animated: true)
    }
}
