
import UIKit
import CommonModule
import SnapKit

public enum NextRoute {
    case login
    case register
}

public protocol WelcomeViewControllerProtocol: AnyObject {
    var completionHandler: ((NextRoute) -> Void)? {get set}
}

public class WelcomeViewController: UIViewController, WelcomeViewControllerProtocol {
    
    // MARK: - UI Components
    
    private lazy var welcomeLabel: UILabel = CustomLabel().configureLabel(text: "Welcome! It’s an app developed by Daniil & Lily", fontSize: 16)
    private lazy var loginButton: UIButton = CustomButton().configureColoredButton(title: "Log in", color: .customBlue, sideInset: 50)
    private lazy var registerButton: UIButton = CustomButton().configureColoredButton(title: "Register", color: .customBlue, sideInset: 50)
    private lazy var stackView: UIStackView = UIStackView()
    
    // MARK: - Navigation
    
    public var completionHandler: ((NextRoute) -> Void)?
    
    // MARK: - Init
    
    public init(completionHandler: ((NextRoute) -> Void)?) {
        self.completionHandler = completionHandler
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    // MARK: - Private
    
    private func configureView() {
        view.backgroundColor = .customWhite
        navigationItem.hidesBackButton = true
        addViews()
        configureButtons()
        configureStackView()
        configureLayouts()
    }
    
    private func addViews() {
        view.addSubview(welcomeLabel)
        view.addSubview(stackView)
    }
    
    private func configureButtons() {
        loginButton.addTarget(self, action: #selector(loginButtonAction), for: .touchUpInside)
        registerButton.addTarget(self, action: #selector(registerButtonAction), for: .touchUpInside)
    }
    
    private func configureStackView() {
        stackView.addArrangedSubview(loginButton)
        stackView.addArrangedSubview(registerButton)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 18
    }
    
    private func configureLayouts() {
        welcomeLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(150)
            make.centerX.equalTo(view.snp.centerX)
        }
        stackView.snp.makeConstraints { make in
            make.top.equalTo(welcomeLabel.snp.bottom).inset(-116)
            make.centerX.equalTo(view.snp.centerX)
            make.height.equalTo(138)
        }
    }
    
    // MARK: - objc
    
    @objc private func loginButtonAction() {
        completionHandler?(.login)
    }
    
    @objc private func registerButtonAction() {
        completionHandler?(.register)
    }
}
