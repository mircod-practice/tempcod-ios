import XCTest
@testable import WelcomeModule

final class WelcomeModuleTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(WelcomeModule().text, "Hello, World!")
    }
}
