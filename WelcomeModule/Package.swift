// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "WelcomeModule",
    platforms: [.iOS(.v15)],
    products: [
        .library(
            name: "WelcomeModule",
            targets: ["WelcomeModule"]),
    ],
    dependencies: [
        .package(path: "../CommonModule"),
        .package(url: "https://github.com/SnapKit/SnapKit.git", branch: "develop"),
    ],
    targets: [
        .target(
            name: "WelcomeModule",
            dependencies: ["CommonModule", "SnapKit"]),
        .testTarget(
            name: "WelcomeModuleTests",
            dependencies: ["WelcomeModule"]),
    ]
)
